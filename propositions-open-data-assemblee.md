# Propositions pour l'open data de l'Assemblée

## Les bonnes pratiques de l'Open Data

Les mises à disposition Open Data réussies remplissent en général 5 critères :

* la **conformité avec les définitions de l'Open Data** (absence de barrières techniques, juridiques et commerciales) et accès simple pour les humains comme les machines ;
* la mise à disposition du **stock des données** : téléchargement du jeu de données intégral afin de permettre des exploitations sur l'ensemble du jeu de données ;
* la mise à disposition du **flux de données** : pour permettre la mise à jour d'un jeu de données déjà exploité ;
* l'**auto-documentation du jeu** : permettre la consultation d'une partie du jeu de données et la comparer à l'utilisation qui en est faite (les données d'un amendement avec l'amendement lui-même)
* des **données auto-suffisantes** : pour maximiser les réutilisations du jeu, il faut éviter d'obliger le chargement de données tierces pour son exploitation ou sa compréhension. La dépendance directe avec d'autres jeux est donc à éviter. Ainsi s'il peut être intéressant de connaître les identifiants d'objets tiers, par exemple pour identifier des homonymies, il est important que le jeu en question contienne les informations signifiantes de ces objets (id du texte de loi + son titre ; id du parlementaire + son nom et son groupe politique ; ...).

Aujourd'hui, si les deux premiers critères sont réalisés par l'Assemblée, les trois suivants ne le sont pas. Dans la suite du document, nous allons décrire des pistes qui permettraient à l'Assemblée d'y parvenir.

## Accès au stock

### Données accessibles depuis chaque page web

Nous souhaiterions que l'Assemblée propose directement depuis la page web produite par l'exploitation de ses bases de données, un lien vers le résultat des requêtes qu'elle a dû effectuer pour obtenir les données publiques présentées sur la page. Il n'est ni nécessaire ni souhaitable de transformer les données. L'idéal est de fournir sous la forme d'un fichier la donnée directement dans le format le plus proche de celui retourné par les requêtes en base (toutes les conversions sont susceptibles d'introduire des erreurs ou des dégradations potentielles).

L'Assemblée réalise déjà ce type de service via la "vue XML" des questions écrites : http://questions.assemblee-nationale.fr/q15/15-578QE.htm/vue/xml Au vu d'une part des besoins partagés par de nombreux réutilisateurs, et d'autre part des complexités liées au traitement des compte-rendus, la mise en place d'une telle disposition pourrait commencer avec les amendements, les dossiers législatifs et les documents parlementaires (textes de lois et rapports).

Cette méthode aurait le mérite d'éviter que les réutilisateurs ne parsent les données des pages HTML comme c'est actuellement le cas pour tous les réutilisateurs que nous connaissons.

Il est à noter que le Parlement canadien a choisi cette méthode de mise à disposition des données.

Nous ne demandons pas à l'Assemblée de générer un schéma spécifique de données à partir du HTML. Ce serait du temps perdu par les services de l'Assemblée, pour produire des données potentiellement de moins bonne qualité que celles que les moissonneurs peuvent extraire directement du HTML. Il s'agit donc bien de rendre directement accessibles les données utilisées pour produire le HTML, sous une forme ouverte la plus simple techniquement pour l'Assemblée, qu'il s'agisse par exemple de XML ou de JSON.

À noter que suite à la table ronde de samedi dernier, Jean-Marc Le Roux, un participant au bureau ouvert, a lancé un prototype de proxy web faisant la conversion de certaines pages du site de l'Assemblée en XML : https://framagit.org/parlement-ouvert/assemblee-nationale.sh. Selon nous l'Assemblée nationale devrait encourager l'existence de tels outils, les mettre en valeur (cf ci-dessous) et éventuellement y participer, plutôt que les développer seule.

### Dumps réguliers au format des données unitaires

Si le format Open Data des données unitaires consultables depuis les pages devait être différent des dumps actuellement disponibles, il faudrait faire évoluer ces dumps quotidiens pour qu'ils contiennent les même données que celles disponibles via l'accès unitaire aux données.

### Dump de la base

Compte tenu du fait que la mise à disposition d'une base de données de quelques Go ne pose pas de problème particulier, nous souhaiterions pouvoir disposer de dumps quotidiens des bases de données publiques (comme le fait le Sénat) en plus des Open Data existantes.

Nous souhaiterions également la mise à disposition d'une image d'exemple de la base de données dans son format natif, pour tester des hypothèses concernant son export ou sa publication en temps réel.

## Données temps réel et flux des modifications

Contrairement aux données DILA qui sont produites une fois par jour, les données de l'Assemblée peuvent potentiellement évoluer à tout moment.

### Mise à disposition d'un flux simple des modifications

Un bon moyen pour permettre une mise à disposition du *flux* serait donc de mettre à disposition un fichier qui signalerait les créations, suppressions ou modifications des objets Open Data, la date de ces actions, et l'url de la donnée concernée. Un simple flux RSS avec suffisamment de champs renseignés et une redondance d'éléments répétés pourrait tout à fait convenir.

Le fichier serait mis à jour en direct dès l'information connue et pourrait être historisé quotidiennement (afin de permettre des synchronisations de base sur des périodicités variées).

### Accès aux amendements en direct : amélioration de l’existant

Le moteur de recherche des amendements dans son format json répond aujourd'hui a beaucoup des besoins liés au suivi quasiment en temps réel des amendements. Officialiser un tel accès serait donc bienvenu tout comme réduire le temps de mise en cache.

Afin de simplifier le suivi en direct, il pourrait être très utile d'y ajouter une métadonnée indiquant la date de dernière édition de chaque amendement.

Il pourrait aussi être utile d'ajouter une option aux arguments de cette API JSON permettant d'activer l'inclusion dans les résultats du texte et/ou de l'exposé des amendements retournés (une restriction plus drastique du nombre maximal de résultats pour les requêtes de ces types serait probablement à prévoir). Il serait aussi souhaitable d'avoir un export CSV directement accessible depuis l'interface du moteur de recherche, comme peut le proposer le Sénat.

Proposer un outil similaire pour les votes serait un plus.

### Mise à disposition du stock en temps réel par un accès direct à la base

L'Assemblée envisage d'ouvrir un accès direct en lecture seule à ses bases des données publiques ou à des flux d'actualisation en temps réel et a évoqué l'idée de proposer des accès authentifiés après signature d'une charte. Nous souhaiterions que ces accès restent le plus ouverts et le moins discriminants possible en permettant facilement l'arrivée de nouveaux entrants.

Il est difficile de donner plus de recommandations sur la manière d'éviter toute discrimination sans engendrer des coûts élevés pour l'Assemblée et tant que l'accès direct à la base de données n'est pas mis en œuvre. En revanche, une piste possible consiste en l'obligation pour les utilisateurs consommant plus d'une certaine quantité de données d'ouvrir leur propre miroir public synchronisé en temps réel. Mais encore faut-il que les choix technologiques faits par l'Assemblée ne limitent pas cet usage : il conviendrait d'éviter que de tels réutilisateurs aient à faire l'acquisition de logiciels propriétaires.

Si les accès authentifiés s'avèrent nécessaires, nous souhaiterions que l'Assemblée propose un compte dont l'identifiant et le mot de passe soient publics. Comme cela, toute personne découvrant le service pourrait le tester immédiatement sans formalité supplémentaire. Ce compte serait mutualisé et son fonctionnement pourraît être parfois dégradé, lorsqu'il est utilisé de manière trop intensive.

## Fichiers open data simplifiés

Les solutions proposées ci-dessus permettent une diffusion complète des données en open data, mais elles ne sont pas adaptées à tous les besoins (particulièrement ceux des non informaticiens). L'Assemblée souhaite aussi proposer des formats simplifiés contenant une partie des données publiques.

Comme les contenus de ces jeux de données simplifiés dépendent des usages qui pourraient en être faits, nous proposons que l'Assemblée encourage des acteurs extérieurs à créer ces jeux de données simplifiés (à partir des données open data complexes) et à les partager.

## Animation de la communauté

### Outil de discussion

Nous proposons que l'Assemblée utilise un site participatif comme un forum, co-géré avec la communauté open data. Pour montrer que ce forum ferait partie des services proposés par l'Assemblée il serait dans le domaine assemblee-nationale.fr, mais pour montrer que ce service est communautaire, expérimental et sans engagement, il serait placé dans un sous domaine intitulé _lab_ et s'appelerait par exemple https://forum.lab.assemblee-nationale.fr.

Un tel service pourrait également accueillir un guichet public des remontées de problèmes rencontrés avec l'open data ou plus largement sur le site de l'Assemblée nationale par les utilisateurs.

Pour simplifier la co-gestion et la co-administration de ce serveur nous proposons que celui-ci soit hébergé et payé par les différents acteurs de la communauté open data.

### Format des réunions

L'Assemblée et les acteurs de l'open data pourraient coorganiser régulièrement des réunions ouvertes (ateliers, bureaux ouverts, datasessions, hackathons...) pour dynamiser ce service.

Il serait intéressant que les futures réunions soient plus horizontales afin de permettre à plus de monde d'exprimer son point de vue. À la suite de notre réunion, des personnes spécialisées dans l'animation d'ateliers participatifs se sont manifestées : elles sont disponibles pour animer de futures réunions.
